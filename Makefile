# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build

.PHONY: all direnv submodule sphinxbuild help Makefile localserver

all: test

help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)


# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)


direnv:
ifeq ("${CI}", "")
ifeq ("${VIRTUAL_ENV}", "")
	@echo >&2 "You must install direnv in order to use 'make' to build the documentation!"
	@echo >&2 "https://github.com/direnv/direnv/blob/master/docs/installation.md"
	@exit 1
endif
endif

submodule:
	@git submodule update --init --recursive

sphinxbuild:
ifeq ("$(shell which sphinx-build || echo 1)", "1")
	./prepare.sh
endif

test: direnv submodule sphinxbuild
	./build-all.sh
	@echo "The build documentation can now be found in the public/-folder."

localserver: test
	@docker run --rm --name yaook-nginx -v ${PWD}/public:/usr/share/nginx/html:ro -p 8080:80 nginx:latest

