# docs.yaook.cloud

**The documentation is available in rendered form at https://beta.docs.yaook.cloud**

This repository holds the build infrastructure
for the Yaook documentation.

It contains the build scripts and submodules
necessary to build a single artifact
containing all of the documentation
the Yaook project has.

- The documentation uses the [Diátaxis][diataxis] framework for structuring
- All documentation can be found on [docs.yaook.cloud][docs-yaook].

The goal of this repository is to have a [single entrypoint][docs-yaook]
with a beautiful page where users and developers can find what they need.
The documentation should be up-to-date (eventually versionable) and heavily
interlinked, with the lowest effort possible.

## Personas

There are two personas defined for the Yaook documentation:

- Developer: contributes code or issue reports to one or more Yaook projects
- User: deploys and maintains Yaook clusters (k8s or OpenStack)

The following persona is explicitly out of scope:

- End-User: The end-user of a cluster deployed with Yaook (be it k8s or OpenStack).
This is the job of upstream documentation (kubernetes / OpenStack).

If any part of Yaook needs end-user documentation (e.g. yaook/k8s) the
documentation should be placed on a separate (sub-)domain.

## Structure of the documentation

The documentation is structured according to the [diátaxis Framework][diataxis]

A quick summary:

Diátaxis separates documentation among two axes:
for studying ("getting to know something") vs. for working ("getting a task done")
and practical vs. theoretical knowledge,
which results in four types of documentation:

- Tutorials (studying + practical)
- How-to guides (working + practical)
- References (working + theoretical)
- Explanations (studying + theoretical)

The following **distinct and disjunct types of documentation** should be
first-class citizens in the Yaook project:

- Tutorials geared toward Users
- How-to guides geared towards Users
- How-to guides geared towards Developers
- Technical references geared towards Users (configuration reference, command line utility references, some CRD references)
- Technical references geared towards Developers (API references, some CRD references)
- Explanations geared towards Users (e.g. how scheduling keys work and why we chose that approach)
- Explanations geared towards Developers (e.g. the concept described in yaook.statemachine.cue)

## Tools

We use Sphinx for everything.

The reason for this is that Sphinx is extremely good at
hyperlinking between different sets of documentation using intersphinx.
It allows us to easily cross-reference for instance the Operation (= User) Guide
for Yaook Operator to the Reference Documentation of yaook/k8s to provide
details about how Rook can be installed.
Or link from the Operation Guide to the Scheduling Key Concept Explanation
and the Scheduling Key Reference for further details. Or from a Tutorial to
the pages which explain concepts which can only be brushed over.

All of that in a way which *does not break on changes*, because the links
point to internal identifiers instead of file names and text titles.

The readability of Sphinx is achieved by using the [Furo theme](https://pradyunsg.me/furo/)
for anything except technical references. For technical references
[Alabaster](https://alabaster.readthedocs.io/en/latest/) or [ReadTheDocs](https://sphinx-rtd-theme.readthedocs.io/en/stable/),
are used, which provide a better rendering of technical references than Furo does (and wants).

To reduce the cognitive load of writing reStructuredText, the [MyST plugin](https://myst-parser.readthedocs.io/en/latest/)
is loaded, which provides extended Markdown syntax to Sphinx,
while allowing to drop down to rST for more complex things.

### Implementation

The docs.yaook.cloud repository contains a Sphinx project and the landing page.
It loads references from related documentation sections of other Yaook projects
via intersphinx and provides shim pages which lead to those.
Because it only contains shims, we disable the search there to not provide a misleading search bar.

The repository includes all other documentation sources as git submodules.
The submodules get updated automatically via a tiggered pipeline or by the
renovate bot (by using the `prepare.sh` script).

All documentation, including the landing page, is built in this repository
and a single artifact is generated from that which encompasses
**all documentation in a single archive** which is then deployed to docs.yaook.cloud.

### Versioning

Currently there is no versioning scheme for the documentation.
Once the Yaook project as a whole has a proper release procedure in place,
the docs.yaook.cloud repository will be versioned to the major releases
so that we can have stable old documentation.

## Specification

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this issue are to be
interpreted *in the spirit of* [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119),
even though we're not technically doing protocol design.

## Content of the docs.yaook.cloud repository

The docs.yaook.cloud repository should contain as much documentation as possible.
Software project specific things should live in the repositories of the
software, in order to have them versioned closely to the respective
software version.

Examples for these software specific documentations are:

- The operator documentation in the [doc directory](https://gitlab.com/yaook/operator/-/tree/devel/doc) of the [Yaook Operator repository](https://gitlab.com/yaook/operator).
- The k8s documentation in the [doc directory](https://gitlab.com/yaook/k8s/-/tree/devel/doc) of the [Yaook K8s repository](https://gitlab.com/yaook/k8s/).

These documentations are included using git submodules.

### Landing page

The landing page in this repository:

- MUST use Sphinx
- MUST be updated by a CI job
- MUST use renovate-bot, CI triggers, or a nightly cronjob depending on what is more suitable

### Developer reference

Developer reference SHOULD be located in a directory named `doc/dev-ref` or
in the respective directory of the Yaook project it is documenting.

- Describe APIs, python modules, code repository directory structures, etc.
- MUST use Sphinx for Python
- SHOULD use Sphinx for non-Python projects if possible
- SHOULD live in the repository of the project it describes
- SHOULD NOT contain cross-project content
- MAY link to references of other projects
- MAY link to the user reference of the same project
- SHOULD use the ReadTheDocs or Alabaster theme
- SHOULD point their intersphinx mapping at the new https://docs.yaook.cloud (to allow for working references in MR builds)

Ideas for Developer How-tos:

- "How to write documentation"
- "How to set up the development environment"
- "How to contribute code"

Ideas for Developer Explanations:

- "Structure of yaook.statemachine"
- "Structure of an Operator"
- "Handling of stateful loads as a developer"
- "What is cue layering and how does it work"

### User reference

User reference SHOULD be located in a directory named `doc/user-ref` or
in the respective directory of the Yaook project it is documenting.

- Describe command line interfaces, CRD structures, configuration options
- SHOULD use Sphinx (see above for rationale)
- SHOULD live in the repository of the project it describes
- MAY be omitted if the tool is self-describing (e.g. `yaookctl --help`), but preferably tools like sphinx-click are used to also have it available in the standard (sphinx) format
- SHOULD point their intersphinx mapping at the new https://docs.yaook.cloud (to allow for working references in MR builds)

Ideas for User How-tos:

- "How to install a management cluster" (from installation-guide)
- "How to deploy a node using metal-controller"
- "How to deploy yaook/k8s from scratch on OpenStack"
- "How to upgrade to a newer OpenStack release"
- "How to recover a broken MariaDB cluster" (from installation-guide)
- "How to use yaookctl to examine databases"
- Other stuff from the installation guide

Ideas for User Explanations:

- "How Yaook handles stateful workloads"
- "How does Yaook do configuration management"

### Tutorials

Developer tutorials SHOULD be located in a directory named `doc/dev-tut`
in the respective directory of the Yaook project it is documenting.

User tutorials SHOULD be located in a directory named `doc/user-tut` in the Yaook project it is documenting.

- Each tutorial MUST be its own Sphinx doctree, possibly even repository

    - That way, there is no distraction along the road when reading a tutorial
    - SHOULD provide a backlink to the tutorial overview (maintained in the docs.yaook.cloud repository) in the navigation with a custom template or so

#### Tutorials ideas

- "Tutorial: Install OpenStack Keystone in Kubernetes using Yaook"
- "Tutorial: Install Kubernetes in OpenStack using yaook/k8s"

### Other content

- MUST use Sphinx
- SHOULD use the Furo theme

[docs-yaook]: https://docs.yaook.cloud/
[diataxis]: https://diataxis.fr/
