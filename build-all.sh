#!/bin/bash
set -euo pipefail
function build_inv() {
    printf "\x1b[1mBuilding inventory for %q\x1b[0m\n ..." "$1"

    path="$1"
    pushd "$path" >/dev/null
    make html
    popd >/dev/null
}

# build once for the objects.inv, build a second time to reload the inventory
build_inv yaook-operator/doc/dev-ref
build_inv yaook-operator/doc/dev-expl
build_inv yaook-operator/doc/user-expl
build_inv yaook-k8s/doc/user-ref
build_inv yaook-k8s/doc/user-guide
build_inv yaook-k8s/doc/user-expl

if [ -z "${YAOOK_DOC_BASEURL:-}" ]; then
    if [ -n "${YAOOK_DOC_LOCAL:-}" ]; then
        baseurl="$(pwd)/public"
    else
        baseurl="https://beta.docs.yaook.cloud"
    fi
else
    baseurl="$YAOOK_DOC_BASEURL"
fi

function mkurl() {
    project="$1"
    section="$2"
    echo "$baseurl/$section/$project/"
}

function mkinventorypath() {
    project="$1"
    section="$2"
    echo "$(pwd)/$project/doc/$section/_build/html/objects.inv"
}

function mkbuildpath() {
    project="$1"
    section="$2"
    echo "$(pwd)/public/$section/$project/"
}

function build_final() {
    project="$1"
    outproject="$2"
    section="$3"
    outpath="$(mkbuildpath "$outproject" "$section")"
    printf "\x1b[1mBuilding documentation section %s for %s\x1b[0m\n ..." "$section" "$project"
    mkdir -p "$outpath"
    pushd "$project/doc/$section" >/dev/null
    sphinx-build . "$outpath"
    popd
}

export YAOOK_INTERSPHINX="{
    \"yaook_operator_dev_ref\": [\"$(mkurl operator dev-ref)\", \"$(mkinventorypath yaook-operator dev-ref)\"],
    \"yaook_operator_user_expl\": [\"$(mkurl operator user-expl)\", \"$(mkinventorypath yaook-operator user-expl)\"],
    \"yaook_operator_dev_expl\": [\"$(mkurl operator dev-expl)\", \"$(mkinventorypath yaook-operator dev-expl)\"],
    \"yaook_k8s_user_ref\": [\"$(mkurl k8s user-ref)\", \"$(mkinventorypath yaook-k8s user-ref)\"],
    \"yaook_k8s_user_expl\": [\"$(mkurl k8s user-expl)\", \"$(mkinventorypath yaook-k8s user-expl)\"],
    \"yaook_k8s_user_guide\": [\"$(mkurl k8s user-guide)\", \"$(mkinventorypath yaook-k8s user-guide)\"]
}"

build_final yaook-operator operator dev-ref
build_final yaook-operator operator user-expl
build_final yaook-operator operator dev-expl
build_final yaook-k8s k8s user-ref
build_final yaook-k8s k8s user-expl
build_final yaook-k8s k8s user-guide

printf '\x1b[1mBuilding umbrella documentation ...\x1b[0m\n'
sphinx-build . public/
printf '\x1b[32;1mBuild completed!\x1b[0m\n'
