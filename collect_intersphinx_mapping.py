import pathlib
import json
import runpy
import sys

SOURCES = [
    "yaook-operator/doc/intersphinx.py",
]

basedir = pathlib.Path(__file__).parent

mapping = {}
for source in SOURCES:
    mapping.update(runpy.run_path(basedir / source)["mapping"])

json.dump(mapping, sys.stdout)
