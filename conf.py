# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import json
import os

project = 'Yaook'
copyright = '2022, The Yaook authors'
author = 'The Yaook authors'
release = 'latest'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser",
    "sphinx_design",
    "sphinx.ext.intersphinx",
    "sphinx_favicon",
]

favicons = [
   {
      "rel": "icon",
      "href": "_static/Husky_blue.svg",
   },
]

templates_path = ['_templates']
exclude_patterns = [
    '_build',
    'Thumbs.db',
    '.DS_Store',
    '.direnv',
    'yaook-operator',
    'yaook-k8s',
]
myst_enable_extensions = ["colon_fence"]

try:
    intersphinx_mapping = json.loads(os.environ['YAOOK_INTERSPHINX'])
except KeyError:
    intersphinx_mapping = json.loads(
        subprocess.check_output("collect_intersphinx_mapping.py"),
    )

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']
html_sidebars = {
    "**": [
        "sidebar/brand.html",
        "sidebar/scroll-start.html",
        "sidebar/navigation.html",
        "sidebar/scroll-end.html",
    ]
}
