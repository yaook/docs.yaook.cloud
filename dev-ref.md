# Developer Reference

::::{grid} 2
:::{grid-item-card}  `yaook/operator`
- {external+yaook_operator_dev_ref:doc}`index`
:::
:::{grid-item-card}  `yaook/k8s`
- [Development & Contribution](https://yaook.gitlab.io/k8s/devel/development/coding-guide.html)
:::
:::{grid-item-card}  `yaook/images`
- [Image Building Policy](/dev-ref/images/01-image-building-policy.md)
:::
::::
