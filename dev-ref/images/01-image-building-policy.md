# Yaook image build policy

* Only one docker image MUST be built within one repository.

  In order to simplify and unify the build logic we build only one image (with different OpenStack releases) within one repository. To share code between the image builds we use a common repository. If the build logic differs for the OpenStack releases, implement this logic preferred within the Dockerfile. Avoid duplicated code.

* All images SHOULD, use `python:3.x-slim-$debiansuite` as base image. The service MUST NOT use any python module from the operating system.

  With the python slim base image we combine a very basic debian image with a python build from source independent of the operating system python packages. Hence, it is easy to switch the python version by just changing the base image.

  At testing or creating your Dockerfile to detect dependencies on the python of the operating system it may be useful to add a `apt-mark hold python2 python3`.
  (Note: The `apt-mark` command SHOULD NOT be included in the resulting Dockerfile(s)).

* The sha256 digest MAY be added to the FROM statement if we do not want to build this image for multiple cpu architectures (e.g. `FROM python:3.9-slim-bullseye@sha256:09c9438d7b13587df2b1b798d372442fb2bdda6cd408aa89dda2e995868c0b31`).

  Without the sum added to the image tag, renovate bot is not able to detect a change in the base image and security fixes might take too long. To determine the most recent digest run the following:

  ```
  docker pull python:3.9-slim-bullseye
  docker inspect --format='{{.RepoDigests}}' python:3.9-slim-bullseye
  ```

  The digest MUST NOT be specified if we want to build the image for multiple cpu architectures as the digest is architecture specific.

* Nova compute images MUST use a normal Ubuntu image to allow using Canonicals vGPU support.

* For OpenStack images at least the `openstack_release` and `branch` arguments MUST be provided.

* The branch arg is constructed out of the `openstack_release`. This is done to unify build logic.

  The OpenStack release MUST NOT be hard-coded into the `branch` argument.

* At most one RUN statement SHOULD be used in a Dockerfile. For multi-stage builds the final stage SHOULD have at most one `RUN` statement.

  This helps to reduce the number of layers and thus potentially reduces image size. There are certainly cases, where multiple Run statements are inevitable.

* Within `RUN` instructions, `set -eux` and semicolon SHOULD be used to concatenate commands (don't use `&&`).

    - By using `set -e` the build will stop as soon as an error (exit-code not 0) occurs. This helps to easier identify the breaking command. Using `&&` would have a similar effect, but it is not as explicit as `set -e`.
    - By using `set -u` undefined variables are detected at build time, preventing surprises in the future.
    - `set -x` provides a better / verbose view of what is happening during the build.

* The `COPY` directive SHOULD be used to copy files to the correct place.

  This improves readability and simplicity.

* After the build files, which are not required during run-time, MUST be cleaned up.

  This reduces the image size and lowers the risk of security holes. This includes for example:

    - removal of files copied into the container by using `rm` in a `RUN` instruction

    - removal of packages not used during runtime

        - introduce a variable named `BUILD_PACKAGES` containing build-related packages at the start of the Dockerfile
        - use `apt-get purge --autoremove -y ${BUILD_PACKAGES}` in a `RUN` instruction at the end of the build section

    - removal of source directories and other files only needed during container image build by using `rm` in a `RUN` instruction

    - removal of cache files

        - There is no need to call `apt-get clean` as there is an apt-hook (`/etc/apt/apt.conf.d/docker-clean`) which handles this automatically.
        - remove apt indices by using `rm` in a `RUN` instruction: e.g. `rm -rf /var/lib/apt/lists/*`
        - where applicable remove cache in user home in a `RUN` instruction, e.g. for the root user by using `rm -rf /root/.cache`

* A `CMD` instruction SHOULD be provided.

* You MUST provide a reason in the Dockerfile comments when using the `ENTRYPOINT` instruction.

* Python VirtualEnv SHOULD NOT be used in the image

  Each image is built for a single service and the only existing Python environment is exclusively used by this service.

* You MUST NOT update the packages in the image using `apt upgrade`.

  Otherwise the hash pinning of the renovate bot will become useless.

* If you use OpenStack source, you SHOULD use the official [opendev.org](https://opendev.org/openstack) repositories.

* If possible you SHOULD add a USER statement at the end of OpenStack related images, so that the container will per default start with the target user (as given in the [documentation](https://docs.yaook.cloud/implementation_details/containers.html)).

* If an image is based on another YAOOK image and add only comparably small difference, both images SHOULD be merged into one image.

  Adding another binary to an image that is used in one case or another is acceptable, as it reduces the maintenance effort.

* If an image is based on another YAOOK image, but the differences are comparably large, each image MUST be built separate repositories.

  As only one image is allowed per repository, the increase of maintenance effort is acceptable here.
