---
hide-doc: true
---

# Welcome to Yaook's documentation!

```{eval-rst}
.. toctree::
    :hidden:
    :caption: User Documentation

    user-tut
    user-ref
    user-guide
    user-expl

.. toctree::
    :hidden:
    :caption: Developer Documentation

    dev-ref
    dev-guide
    dev-expl
```

::::{grid} 1
:::{grid-item-card}  Getting Started: Tutorials
:link: user-tut
:link-type: doc
The tutorials are meant to give you a kickstart in learning about Yaook.
:::
::::

::::{grid} 2
:::{grid-item-card}  User Reference
:link: user-ref
:link-type: doc
Technical reference documentation of Yaook from the user perspective.
:::
:::{grid-item-card}  Operation Guide
:link: user-guide
:link-type: doc
Keep this under your pillow when *running* Yaook clusters.
:::
::::

::::{grid} 2
:::{grid-item-card}  Developer Reference
:link: dev-ref
:link-type: doc
Extensive API and internals reference documentation for developers.
:::
:::{grid-item-card}  Developer Guide
:link: dev-guide
:link-type: doc
Guidelines and how-tos for contributing to Yaook.
:::
::::

::::{grid} 2
:::{grid-item-card}  Understanding Yaook
:link: user-expl
:link-type: doc
In-depth and extensive discussions and explanations about how (and why) Yaook works.
:::
:::{grid-item-card}  Understanding the Yaook Code
:link: dev-expl
:link-type: doc
In-depth explanations of the Yaook code and its structure.
:::
::::
