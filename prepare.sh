#!/bin/bash
set -euo pipefail
pip install -r requirements.txt
pushd yaook-operator >/dev/null
git checkout feature/doc-revamp || echo 'failed to find doc-revamp branch...?'
pip install -e .
popd
pushd yaook-k8s >/dev/null
git checkout feature/doc-revamp || echo 'failed to find doc-revamp branch...?'
popd

